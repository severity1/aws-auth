#!/bin/bash

APPNAME=aws-auth
ARCH="amd64"
PLATFORM=$1 # can be "darwin", "windows", "linux"
ARTIFACTSDIR=${CI_PROJECT_DIR}/${ARTIFACTS_DIR}

mkdir -p ${ARTIFACTSDIR}

OUT=${APPNAME}
if [ $PLATFORM = "windows" ]; then
  OUT+=".exe"
fi

echo "Building ${APPNAME} for ${PLATFORM}_${ARCH}"
env GOOS=${PLATFORM} GOARCH=${ARCH} go build -o ${ARTIFACTSDIR}/${PLATFORM}_${ARCH}/${OUT}
