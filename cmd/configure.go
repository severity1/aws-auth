package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

// configCmd represents the config command
var configureCmd = &cobra.Command{
	Use:   "configure",
	Short: "Wizard to generate config file.",
	Long:  `Wizard to generate your ~/.aws-auth.yaml config file`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("configure called")
	},
}

func init() {
	rootCmd.AddCommand(configureCmd)
}
