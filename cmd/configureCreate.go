package cmd

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// createCmd represents the create command
var createCmd = &cobra.Command{
	Use:   "create",
	Short: "Sub-command to create the ~/aws-auth.yaml file.",
	Long: `Sub-command to create the ~/aws-auth.yaml file.
		This command will prompt for inputs, your inputs
		will be used to populat ethe config file.
	`,
	Run: func(cmd *cobra.Command, args []string) {
		// If a config file is found, read it in.
		if err := viper.ReadInConfig(); err == nil {
			fmt.Println("Already using config file:", viper.ConfigFileUsed())
			os.Exit(1)
		}
		fmt.Println("Creating new config file.")

		newCfg := viper.New()
		if cfgFile != "" {
			// Use config file from the flag.
			newCfg.SetConfigFile(cfgFile)
		} else {
			// Find home directory.
			home, err := homedir.Dir()
			if err != nil {
				fmt.Println(err)
				os.Exit(1)
			}

			// Search config in home directory with name ".aws-auth" (without extension).
			newCfg.AddConfigPath(home)
			newCfg.SetConfigType("yaml")
			newCfg.SetConfigName(".aws-auth")
			newCfg.SetConfigFile(home + string(os.PathSeparator) + ".aws-auth.yaml")
		}

		newCfg.AutomaticEnv() // read in environment variables that match

		// Input Name
		readName := bufio.NewReader(os.Stdin)
		fmt.Print("Enter main name: ")
		mainName, _ := readName.ReadString('\n')
		mainName = strings.TrimSpace(mainName)
		newCfg.Set("main.name", mainName)

		// Input Role
		readRole := bufio.NewReader(os.Stdin)
		fmt.Print("Enter main role: ")
		mainRole, _ := readRole.ReadString('\n')
		mainRole = strings.TrimSpace(mainRole)
		newCfg.Set("main.role", mainRole)

		// Input Username
		readUsername := bufio.NewReader(os.Stdin)
		fmt.Print("Enter main username: ")
		mainUsername, _ := readUsername.ReadString('\n')
		mainUsername = strings.TrimSpace(mainUsername)
		newCfg.Set("main.username", mainUsername)

		// Input AccountID
		readAccountID := bufio.NewReader(os.Stdin)
		fmt.Print("Enter main account id: ")
		mainAccountID, _ := readAccountID.ReadString('\n')
		mainAccountID = strings.TrimSpace(mainAccountID)
		newCfg.Set("main.account_id", mainAccountID)

		// Input Account Name
		readAccountName := bufio.NewReader(os.Stdin)
		fmt.Print("Enter main account name: ")
		mainAccountName, _ := readAccountName.ReadString('\n')
		mainAccountName = strings.TrimSpace(mainAccountName)
		newCfg.Set("main.account_name", mainAccountName)

		//  Input accounts to assume
		cont := 0
		for cont == 0 {
			// Input Account to Assume
			readAssumeIdentifier := bufio.NewReader(os.Stdin)
			fmt.Print("Enter identifier: ")
			assumeIdentifier, _ := readAssumeIdentifier.ReadString('\n')
			assumeIdentifier = strings.TrimSpace(assumeIdentifier)

			// Input Account ID
			readAssumeID := bufio.NewReader(os.Stdin)
			fmt.Print("Enter id: ")
			assumeID, _ := readAssumeID.ReadString('\n')
			assumeID = strings.TrimSpace(assumeID)
			assumeIDKey := "accounts" + "." + assumeIdentifier + "." + "id"
			newCfg.Set(assumeIDKey, assumeID)

			var prompt string
			fmt.Print("Add more accounts to assume? (y/N): ")
			_, err := fmt.Scan(&prompt)
			if err != nil {
				fmt.Print(err)
			}
			prompt = strings.TrimSpace(prompt)
			prompt = strings.ToLower(prompt)

			if prompt == "y" {
				cont = 0
			} else if prompt == "n" {
				cont = 1
			} else {
				cont = 1
			}
		}

		if err := newCfg.WriteConfigAs(newCfg.ConfigFileUsed()); err != nil {
			fmt.Println(err.Error())
		}

		fmt.Println("Configuration is done! Please review your config file.")
	},
}

func init() {
	configureCmd.AddCommand(createCmd)
}
