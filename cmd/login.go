package cmd

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	credentials "gitlab.com/severity1/aws-auth/lib/credentials"
	login "gitlab.com/severity1/aws-auth/lib/login"
)

var username string

// loginCmd represents the login command
var loginCmd = &cobra.Command{
	Use:   "login",
	Short: "Login to AWS using an IAM account will prompt for MFA input",
	Long: `Login to AWS using an IAM account will prompt for MFA input.
		This will update or add a section to your ~/.aws/credentials file
		based on your configs.`,
	Run: func(cmd *cobra.Command, args []string) {
		// Test if username exists
		if viper.GetString("main.username") == "" {
			fmt.Println("could not find username")
			os.Exit(1)
		}

		// Input MFA
		readMfa := bufio.NewReader(os.Stdin)
		fmt.Print("Enter MFA code: ")
		mfa, _ := readMfa.ReadString('\n')
		mfa = strings.TrimSpace(mfa)

		// Test if MFA is set
		if len(mfa) == 0 {
			fmt.Println("MFA not set")
			os.Exit(1)
		}

		// Load viper configs
		accountID := viper.GetString("main.account_id")
		accountName := viper.GetString("main.account_name")
		userName := viper.GetString("main.username")
		durationSeconds := viper.GetInt64("main.duration")
		if durationSeconds == 0 {
			durationSeconds = 43200
		}
		serialNumber := "arn:aws:iam::" + accountID + ":mfa/" + userName

		accessKeyID, secretAccessKey, sessionToken := login.Generate(
			durationSeconds,
			serialNumber,
			mfa,
		)

		credentials.Create(
			accountName,
			accessKeyID,
			secretAccessKey,
			sessionToken,
		)
	},
}

func init() {
	rootCmd.AddCommand(loginCmd)
	// Username flag definition
	loginCmd.Flags().StringVarP(&username, "username", "u", "", "Username you use to login to AWS. (not required)")
	viper.BindPFlag("main.username", loginCmd.Flags().Lookup("username"))
}
