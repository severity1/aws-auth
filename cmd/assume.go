package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	assume "gitlab.com/severity1/aws-auth/lib/assume"
	credentials "gitlab.com/severity1/aws-auth/lib/credentials"
)

var account string

// assumeCmd represents the assume command
var assumeCmd = &cobra.Command{
	Use:   "assume",
	Short: "Assume a role in another acount",
	Long: `Assume a role in another acount using the temporary credentials
		you generated using the login sub-command`,
	Run: func(cmd *cobra.Command, args []string) {
		// Test if accounts section exists
		if viper.InConfig("accounts") != true {
			fmt.Println("Accounts section is missing in config file.")
			os.Exit(1)
		}
		// Test if accounts section is populated
		if viper.Get("accounts") == nil {
			fmt.Println("No accounts to assume in config file.")
			os.Exit(1)
		}

		// Load Target account details from viper config
		mainAccountName := viper.GetString("main.account_name")

		accountID := "accounts" + "." + account + "." + "id"
		targetAccountID := viper.GetString(accountID)

		accountName := "accounts" + "." + account + "." + "name"
		targetAccountName := viper.GetString(accountName)

		accountRole := "accounts" + "." + account + "." + "role"
		targetAccountRole := viper.GetString(accountRole)

		accountDuration := "accounts" + "." + account + "." + "duration"
		targetAccountDuration := viper.GetInt64(accountDuration)
		if targetAccountDuration == 0 {
			targetAccountDuration = 900
		}

		accessKeyID, secretAccessKey, sessionToken := assume.Generate(
			mainAccountName,
			targetAccountDuration,
			targetAccountID,
			targetAccountRole,
		)

		credentials.Create(
			targetAccountName,
			accessKeyID,
			secretAccessKey,
			sessionToken,
		)
	},
}

func init() {
	rootCmd.AddCommand(assumeCmd)

	// Account flag definition
	assumeCmd.Flags().StringVarP(&account, "account", "a", "", "Account you want to assume. (required)")
	assumeCmd.MarkFlagRequired("account")
}
