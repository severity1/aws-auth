package credentials

import (
	"fmt"
	"os"

	"github.com/alyu/configparser"
	homedir "github.com/mitchellh/go-homedir"
)

// Create This func must be Exported, Capitalized, and comment added.
func Create(
	accountName string,
	accessKeyID string,
	secretAccessKey string,
	sessionToken string,
) {
	// Validate AWS credentials file
	credsFile, err := homedir.Expand("~/.aws/credentials")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	// Read AWS credentials file for parsing
	configparser.Delimiter = "="
	creds, err := configparser.Read(credsFile)
	if err != nil {
		fmt.Println(err)
	}

	// Get our target section if it does not exist create it and set values
	section, err := creds.Section(accountName)
	if err != nil {
		fmt.Println("Could not find section, creating", accountName)
		section = creds.NewSection(accountName)
		section.Add("aws_secret_access_key", secretAccessKey)
		section.Add("aws_access_key_id", accessKeyID)
		section.Add("aws_session_token", sessionToken)
	} else {
		fmt.Println("Found section", accountName)
		section.SetValueFor("aws_secret_access_key", secretAccessKey)
		section.SetValueFor("aws_access_key_id", accessKeyID)
		section.SetValueFor("aws_session_token", sessionToken)
	}
	fmt.Println("Credentials generated!")

	err = configparser.Save(creds, credsFile)
	if err != nil {
		fmt.Println(err)
	}
}
