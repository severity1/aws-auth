package assume

import (
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sts"
)

// Generate This func must be Exported, Capitalized, and comment added.
func Generate(
	accountName string,
	accountDuration int64,
	accountID string,
	accountRole string,
) (
	accessKeyID string,
	secretAccessKey string,
	sessionToken string,
) {
	// Create a Session using the account in the main section of your config file.
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		Profile: accountName,
	}))
	svc := sts.New(sess)
	input := &sts.AssumeRoleInput{
		DurationSeconds: aws.Int64(accountDuration),
		ExternalId:      aws.String(accountID),
		RoleArn:         aws.String(accountRole),
		RoleSessionName: aws.String(accountID),
	}

	// Assume the target role
	result, err := svc.AssumeRole(input)
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case sts.ErrCodeMalformedPolicyDocumentException:
				fmt.Println(sts.ErrCodeMalformedPolicyDocumentException, aerr.Error())
			case sts.ErrCodePackedPolicyTooLargeException:
				fmt.Println(sts.ErrCodePackedPolicyTooLargeException, aerr.Error())
			case sts.ErrCodeRegionDisabledException:
				fmt.Println(sts.ErrCodeRegionDisabledException, aerr.Error())
			default:
				fmt.Println(aerr.Error())
			}
		} else {
			// Print the error, cast err to awserr.Error to get the Code and
			// Message from an error.
			fmt.Println(err.Error())
		}
		return
	}
	accessKeyID = *result.Credentials.AccessKeyId
	secretAccessKey = *result.Credentials.SecretAccessKey
	sessionToken = *result.Credentials.SessionToken

	return accessKeyID, secretAccessKey, sessionToken
}
