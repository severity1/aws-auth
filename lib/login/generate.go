package login

import (
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sts"
)

// Generate This func must be Exported, Capitalized, and comment added.
func Generate(
	accountDuration int64,
	serialNumber string,
	tokenCode string,
) (
	accessKeyID string,
	secretAccessKey string,
	sessionToken string,
) {
	// Create STS session
	svc := sts.New(session.New())
	input := &sts.GetSessionTokenInput{
		DurationSeconds: aws.Int64(accountDuration),
		SerialNumber:    aws.String(serialNumber),
		TokenCode:       aws.String(tokenCode),
	}

	// Get STS Session
	result, err := svc.GetSessionToken(input)
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case sts.ErrCodeRegionDisabledException:
				fmt.Println(sts.ErrCodeRegionDisabledException, aerr.Error())
			default:
				fmt.Println(aerr.Error())
			}
		} else {
			// Print the error, cast err to awserr.Error to get the Code and
			// Message from an error.
			fmt.Println(err.Error())
		}
		return
	}
	accessKeyID = *result.Credentials.AccessKeyId
	secretAccessKey = *result.Credentials.SecretAccessKey
	sessionToken = *result.Credentials.SessionToken
	return accessKeyID, secretAccessKey, sessionToken
}
