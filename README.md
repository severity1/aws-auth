# aws-auth blah
A helper script to manage your ~/.aws/credentials file in an MFA enforced environment and multi-account setup (AWS Organizations). Written in Go.

## Scenario
You are required to write apps that will interact with an S3 bucket so you asked your AWS Administrator to create an IAM user account for you under your company's AWS Account.

Your AWS Administrator gave you an IAM user account to your root/login-only AWS account, he gave you an IAMRole to assume in another account (sandbox|dev account),
he/she told you that the s3 bucket that you need to read/write on is in that other account (sandbox|dev account) which you need to assume.

## Installation
Just download the binaries for your platform from the list below:
* [windows](https://gitlab.com/severity1/aws-auth/-/jobs/artifacts/master/download?job=windows)
* [darwin](https://gitlab.com/severity1/aws-auth/-/jobs/artifacts/master/download?job=darwin)
* [linux](https://gitlab.com/severity1/aws-auth/-/jobs/artifacts/master/download?job=linux)

Once downloaded, put it in your executable path.

## Create your aws-auth config file
* Login to your AWS Console using the credentials assigned to you and
navigate to your IAM User account and generate your Access Keys.

* Copy the generated access and secret keys and put it inside your `~/.aws/credentials` similar to the one below;
    ```bash
    [default]
    aws_secret_access_key = xxxxxxxxxxxxxxxxxxx
    aws_access_key_id = XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    ```

* Manually create `~/.aws-auth.yaml`, populate it with details similar to the one below.
    ```bash
    main:
      name:         Test          # Some made up name
      role:         TestRole      # The Role assigned to you.
      username:     severity1     # your actual AWS IAM username.
      account_id:   xxxxxxxxxxxx  # the account id of the AWS Account you logged-in from the steps above.
      account_name: companyX-root # some made up name to identify your account-id
    accounts:
      sandbox:
        id: severity1-sandbox                          # some made up identifier
        name: companyX-sandbox                         # some made up name
        role: arn:aws:iam::xxxxxxxxxxxx:role/TestRole1 # The arn of the role you want to assume in the target account
        duration: 3600                                 # the duration of your assume session
      ops:
        id: severity1-ops
        name: companyX-ops
        role: arn:aws:iam::xxxxxxxxxxxx:role/TestRole2
        duration: 3600
      someaccount:
        id: severity1-someaccount
        name: companyX-someaccount
        role: arn:aws:iam::xxxxxxxxxxxx:role/SomeRole
        duration: 3600
    ```


## Usage
1. Login with MFA, this will prompt for MFA input.
    ```bash
    aws-auth login               # login using the username in the config
    aws-auth login -u john.smith # login using another username
    ```

2. This should start populating your `~/.aws/credentials` similar to the one below.
    ```bash
    [default]
    aws_secret_access_key = xxxxxxxxxxxxxxxxxxx
    aws_access_key_id = XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

    [companyX-root]
    aws_secret_access_key = xxxxxxxxxxxxxxxxxxx
    aws_access_key_id = XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    aws_session_token = xxxxxxxxxxxxxXXXXxxxxxxxxxxx
    ```

3. This ensures that you are now logged-in with MFA. Some setups will disallow assuming of roles in another AWS account if your principal IAM user account is not logged-in with MFA.

4. Next, we assume a Role in another account using the account from step 1.
    ```bash
    aws-auth assume sandbox # assume the sandbox role
    aws-auth assume ops     # assume the ops role
    ```

  * This should start populating your ~/.aws/credentials similar to the one below.
      ```bash
      [default]
      aws_secret_access_key = xxxxxxxxxxxxxxxxxxx
      aws_access_key_id = XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

      [companyX-root]
      aws_secret_access_key = xxxxxxxxxxxxxxxxxxx
      aws_access_key_id = XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      aws_session_token = xxxxxxxxxxxxxXXXXxxxxxxxxxxx

      [companyX-sandbox]
      aws_secret_access_key = xxxxxxxxxxxxxxxxxxx
      aws_access_key_id = XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      aws_session_token = xxxxxxxxxxxxxXXXXxxxxxxxxxxx

      [companyX-ops]
      aws_secret_access_key = xxxxxxxxxxxxxxxxxxx
      aws_access_key_id = XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      aws_session_token = xxxxxxxxxxxxxXXXXxxxxxxxxxxx

      [companyX-someaccount]
      aws_secret_access_key = xxxxxxxxxxxxxxxxxxx
      aws_access_key_id = XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      aws_session_token = xxxxxxxxxxxxxXXXXxxxxxxxxxxx
      ```

  * Now when you write your code you just have to specify the profile `[companyX-sandbox]` to use the AWS services in that account. No need to hard-code credentials together with your code and accidentally committing it to git or svn.

  * If you are using `aws-cli` just pass in the `--profile companyX-someaccount` parameter to use that specific account or if you are using the AWS SDK for your language of choice there should be a similar parameter you can use. See examples below;

    * **aws-cli example**

        ```bash
        aws --profile companyX-someaccount s3 ls
        aws --profile companyX-someaccount s3 cp myfile.txt s3://somebucket/myfile.txt
        ```

    * **python AWS SDK example**

        ```python
        # Connect to s3 api and publish to deploy bucket inside companyX-someaccount
        conn = boto3.Session(
          profile_name='companyX-someaccount',
          region_name='ap-southeast-2'
        )
        s3_client = conn.client('s3')
        upload = s3_client.upload_file(
          'myfile.txt',
          'somebucket_test',
          'somepath/myfile.txt',
          ExtraArgs={'ServerSideEncryption': "AES256"}
        )
        ```

    * **Golang AWS SDK example**

        ```go
        // The session the S3 Uploader will use
        sess := session.Must(session.NewSessionWithOptions(session.Options{
          Profile: 'companyX-someaccount',
        }))

        // Create an uploader with the session and default options
        uploader := s3manager.NewUploader(sess)
        f, err  := os.Open(filename)
        if err != nil {
            return fmt.Errorf("failed to open file %q, %v", filename, err)
        }

        // Upload the file to S3.
        result, err := uploader.Upload(&s3manager.UploadInput{
            Bucket: aws.String(myBucket),
            Key:    aws.String(myString),
            Body:   f,
        })
        ```

## Notes
Currently the `aws-auth login` command is good for 12 hours so you only have to input MFA once every 12 hours.

The `aws-auth assume`'s duration is based on the `duration` parameter in the `~/.aws-auth.yaml` config file, in our example's case an assume is good for 3600 seconds, you might need to run this command multiple times during your development, re-running this command will refresh your tokens and will reset the duration.

## Todos
* Add actual tests.
* Working implementation of the `aws-auth configure create` config file creation wizard.
* Implement input validation.
* Streamline code.
* CI/CD Improvements.
